import argparse
import contextlib
import sys

import cairo

class Pager:
    def __init__(self, inf, nrows, skip=None):
        self._inf = inf
        self._nrows = nrows
        self._held = None
        self._eof = False
        self._skip = skip

    def __iter__(self): return self

    def __next__(self):
        if self._eof and self._held is None: raise StopIteration()

        rows = [self._held] if self._held is not None else []
        self._held = None

        while len(rows) < self._nrows:
            while True:
                r = self._inf.readline()
                if (not r or self._skip is None or not r.startswith(self._skip)): break

            if not r:
                self._eof = True
                if rows:
                    return rows
                else:
                    raise StopIteration()

            if r[0] == '\u000c':
                rest = r[1:].rstrip()
                if rest: self._held = rest
                return rows

            rows.append(r.rstrip())

        return rows


def output(inf,
           of,
           background,
           font='PT Mono',
           fontsize=12,
           textalpha=0.6,
           width = 210 / 25.4 * 72,
           height = 297 / 25.4 * 72,
           baselineskip = 12,
           xoffset=0):

    s = cairo.PDFSurface(of, width, height)
    c = cairo.Context(s)

    rows = int(height / baselineskip)
    pager = Pager(inf, rows, skip='\u0001')

    margin = (210 / 25.4 - 8) / 2 * 72

    if xoffset != 0:
        c.select_font_face(font)
        c.set_font_size(fontsize)
        space_ext = c.text_extents(' ')
        margin += xoffset * space_ext.x_advance

    for page in pager:
        background(c, width, height, baselineskip)
        c.select_font_face(font)
        c.set_font_size(fontsize)
        c.set_source_rgba(0, 0, 0, textalpha)

        for row, text in enumerate(page):
            c.move_to(margin, row * baselineskip + baselineskip - 3)
            c.show_text(text)

        s.flush()
        s.show_page()

    s.finish()

def white(c, width, height, baselineskip):
    pass

def bluebar(c,
            width, height, baselineskip,
            blocksize = 3,
            rule = 1,
            white = (1, 1, 1),
            blue = (0.910, 0.976, 1)):

    rows = int(height / baselineskip)

    c.select_font_face('Clear Sans')
    c.set_font_size(8)
    c.set_fill_rule(cairo.FillRule.EVEN_ODD)

    for row in range(rows):
        if row % (2 * blocksize) < blocksize:
            # In white area. Draw blue hrule.
            vpos = baselineskip - rule
            barheight = rule
            colour = blue
            digit_colour = [2 * x - 1 for x in blue]
        else:
            # In blue area. Draw blue bars.
            vpos = 0
            barheight = baselineskip - rule
            colour = blue
            digit_colour = white

        c.rectangle(0, row * baselineskip + vpos, width, barheight)
        c.set_source_rgba(*colour, 1)
        c.fill()

        if row % 5 == 0:
            c.set_source_rgba(*digit_colour, 1)
            c.move_to(5, row * baselineskip + baselineskip - 5)
            c.show_text(str(1 + row))


def greenbar(c,
            width, height, baselineskip,
            blocksize=3,
            rule=1,
            light=(0.929, 0.988, 0.902),
            dark=(0.572, 0.796, 0.447)):

    blockheight = blocksize * baselineskip
    blocks = int(height / blockheight) + 1

    c.select_font_face('Clear Sans')
    c.set_font_size(8)
    c.set_fill_rule(cairo.FillRule.EVEN_ODD)
    c.set_line_width(rule)

    for block in range(0, blocks, 2):
        c.set_source_rgba(*light, 1)
        c.rectangle(0, block * blockheight, width, blockheight)
        c.fill()

        c.set_source_rgba(*dark, 1)
        c.rectangle(0, block * blockheight, width, rule)
        c.fill()
        c.rectangle(0, (block + 1) * blockheight - rule, width, rule)
        c.fill()

    c.set_source_rgba(*dark, 1)
    for row in range(int(height / baselineskip)):
        c.move_to(0, row * baselineskip + baselineskip - 5)
        c.show_text('{:2d}'.format(1 + row))


def main(argv):
    backgrounds = {
        'blue': bluebar,
        'green': greenbar,
        'white': white
    }

    ap = argparse.ArgumentParser()

    ap.add_argument('-a', '--alpha', type=float, default=0.6)
    ap.add_argument('-b', '--background', choices=backgrounds.keys(), default='blue')
    ap.add_argument('-B', '--baselineskip', type=float, default=None)
    ap.add_argument('-f', '--font', type=str, default='PT Mono')
    ap.add_argument('-s', '--font-size', type=float, default=12)
    ap.add_argument('-m', '--margin', type=int, default=0)
    ap.add_argument('-o', '--output', type=str, required=True)
    ap.add_argument('-W', '--width', type=float, default=210.0)
    ap.add_argument('-H', '--height', type=float, default=297.0)
    ap.add_argument('input', type=str, nargs='?')

    args = ap.parse_args(argv[1:])

    bls = args.baselineskip if args.baselineskip is not None else args.font_size

    with (open(args.output, 'wb') as f,
          contextlib.ExitStack() as cstack):

        if args.input:
            inf = cstack.enter_context(open(args.input, 'r'))
        else:
            inf = sys.stdin

        output(inf, f, backgrounds[args.background],
               width=args.width / 25.4 * 72,
               height=args.height / 25.4 * 72,
               baselineskip=bls,
               font=args.font,
               fontsize=args.font_size,
               xoffset=args.margin,
               textalpha=args.alpha)

if __name__ == '__main__':
    main(sys.argv)
